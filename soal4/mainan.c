#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>

// int main(int argc, char argv[]){
int main() {
    
pid_t pid, sid;    // Variabel untuk menyimpan PID

pid = fork();     // Menyimpan PID dari Child Process

    char cron[1024];
    char file_name[100];
    int len;

    fgets(cron, 1024, stdin);
    scanf("%s", file_name);

    // if (argc == 3) {
    //     file_name = argv[2];
    
    //     snprintf(cron, sizeof(cron), "%s", argv[1]);
    // 

    int values[5][60] = {0};

    len = strlen(file_name);
    if (len > 3 && strcmp(file_name + len - 3, ".sh") == 0) {
        printf("directory is valid.\n");
    } else {
        printf("eror directory is not valid.\n");
    }

    char* fields[5];
    char* p = strtok(cron, " \t\n");
    int i = 0;

    while (p != NULL && i < 5) {
        fields[i++] = p;
        p = strtok(NULL, " \t\n");
    }
    if (i < 5) {
        printf("eror cron expression\n");
        return 1;
    }

    for (int i = 0; i < 5; i++) {
        if (strcmp(fields[i], "*") == 0) {
            for (int j = 0; j < 60; j++) {
                values[i][j] = 1;
            }
        } else if (strchr(fields[i], '/') != NULL) {
            char* p = strchr(fields[i], '/');
            int step = strtol(p+1, NULL, 10);
            *p = '\0';

            int start = (strcmp(fields[i], "*") == 0) ? 0 : strtol(fields[i], NULL, 10);
            for (int j = start; j < 60; j += step) {
                values[i][j] = 1;
            }
            
        } else if (strchr(fields[i], ',') != NULL) {
            char* p = fields[i];
            while (*p != '\0') {
                char* end;
                int value = strtol(p, &end, 10);
                if (end == p) {
                    printf("eror cron expression\n");
                    return 1;
                }
                values[i][value] = 1;
                if (*end == '\0') {
                    break;
                }
                p = end + 1;
            }
        } else {
            int value = strtol(fields[i], NULL, 10);
            if (value < 0 || (i == 0 && value > 59) || (i == 1 && value > 23) || (i == 2 && value > 31) || (i == 3 && value > 12) || (i == 4 && value > 6)) {
                printf("eror cron expression\n");
                return 1;
            }
            values[i][value] = 1;
        }
    }

    printf("Valid cron expression\n");
    return 0;
   
 

  /* Keluar saat fork gagal
  * (nilai variabel pid < 0) */
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  /* Keluar saat fork berhasil
  * (nilai variabel pid adalah PID dari child process) */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/")) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

    while(1){  

        sleep(30);
    }
}

//30 4 * * 1 root/backup.sh
//ps aux |grep "mainan"
