# sisop-praktikum-modul-2-2023-HS-E06

## Praktikum Modul-2 Sistem Operasi


- 5025211049	Zakia Kolbi
- 5025211210	Nadiah Nuri Aisyah
- 5025211216	Akmal Nafis

# Nomor 1



a. Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

```bash
      execlp("wget", "wget", "--no-check-certificate", url , "-O", filename, "-q", NULL);

```
untuk mendownload disini kami menggunakan fungsi `execlp` untuk menjalankan perintah `wget` untuk mendownload file yang ada pada link yang disimpan pada `char *url` ."--no-check-certificate" adalah Opsi untuk menonaktifkan  verifikasi sertifikat SSL pada saat mengunduh file dan `"-O"` untuk menyimpan file unduhan.

```bash
    execlp("unzip", "unzip", "-qq", filename, NULL);
```
karena men unzip tidak boleh menggunkan system maka menggunakan fungsi c `execlp` dan menjalankan perintah `unzip` untuk file yang bernama `Binatang.zip` yang ada di char *filename={};

b. Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

```bash
    system("display $(ls |ls *.jpg | shuf -n1)");
```
Sebelum dipilih maka disini menggunakan `ls *.jpg` agar memfilter file dengan format .jpg,kemudian untuk memilih gambar yang sudah diunzip secara random menggunakan `shuf -n1` yang artinya shuffle satu baris file. Kemudian menggunakan fungsi `system` untuk menjalankan perintah `display` untuk menampilkan gambar acak yang sudah dipilih.

c. Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

```bash 
     execlp("mkdir", "mkdir", "-p", foldername[0], foldername[1], foldername[2], NULL);
```
pertama menggunakan fungsi `execlp` untuk menjalankan perintah `mkdir` untuk membuat folder berurutan dengan nama yang ada pada `char *foldername[] = {"HewanDarat", "HewanAmphibi", "HewanAir"};` setelah itu dilakukan proses pemindahan dengan menggunakan fungsi system untuk menjalankan perintah `mv` untuk memilahnya maka dipindah sesuai nama terakhir gambarnya /sesuai jenis hewannya, dan dimasukkan ke folder yang sudah dibentuk sebelumnya
```bash
      system("mv *darat.jpg /home/akmalx/Praktikum_sisop/modul2/no1/HewanDarat " );
      system("mv *amphibi.jpg /home/akmalx/Praktikum_sisop/modul2/no1/HewanAmphibi " );
      system("mv *air.jpg /home/akmalx/Praktikum_sisop/modul2/no1/HewanAir ");
```

d. Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.
```bash
    execlp("zip", "zip", "-r", "hewan.zip", foldername[0], foldername[1], foldername[2], NULL);
``` 
menggunakan fungsi execlp untuk menjalankan perintah `zip` dengan `-r` agar di proses secara rekursif dengan nama zip berupa `hewan.zip`, isi file zip merupakan folder jenis hewan sesuai nama foldername. Pada fungsi delete file digunakan `system("rm -rf Hewan* Binatang.zip" );` untuk menghapus file hasil download pada langkah pertama.

```bash
 (chdir("/home/akmalx/Praktikum_sisop/modul2/no1")
```
digunakan agar program diproses di direktori tersebut

Catatan : untuk melakukan zip dan unzip tidak boleh menggunakan system
proses 

![image](https://cdn.discordapp.com/attachments/853191323589541908/1094245543816990791/image.png)
Download Binatang.zip kemudian ekstrak pilih secara acak kemudian tampilkan

![image](https://cdn.discordapp.com/attachments/853191323589541908/1094245915067428944/image.png)
hasil zip dari folder jenis Binatang menjadi hewan.zip

![image](https://cdn.discordapp.com/attachments/853191323589541908/1094246223449432084/image.png)
isi dari hewan.zip berupa jenis tipe hewan

![image](https://cdn.discordapp.com/attachments/853191323589541908/1094246638136070165/image.png)
file yang ada didalam folder hewan.zip



# Nomor 2
a. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
```bash
void createFolder(char TIMESTAMP[]){
    mkdir(TIMESTAMP, 0777);
}
```
```bash
int main() {

    while(1){
        time_t t = time(NULL); 
        time_t current_time;
        time(&current_time);
        char TIMESTAMP[22];
        strftime(TIMESTAMP, sizeof(TIMESTAMP), "[%Y-%m-%d_%H:%M:%S]", localtime(&current_time));

        createFolder(TIMESTAMP);
```
b. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
```bash
void downloadImage(char* url, char* foldername) {
    time_t t = time(NULL); 
    time_t current_time;
    time(&current_time);
    char path[200];
    char command[500];

    char TIMESTAMP[22];
    strftime(TIMESTAMP, sizeof(TIMESTAMP), "[%Y-%m-%d_%H:%M:%S]", localtime(&current_time));

    char filename[30];
    sprintf(filename, "%s.jpg", TIMESTAMP);

    sprintf(path, "%s/%s", foldername, filename);

    pid_t id = fork();
    if(id==0){
        execlp("wget", "wget", url, "-O", path, NULL);
        exit(0);
    }
    else if(id > 0){
        int status;
        waitpid(id, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("File berhasil diunduh\n");
        } else {
            printf("File gagal diunduh\n");
        }
    }
    
}
```
```bash
int i;
        for(i = 0; i<15; i++){
            char url[50];
            int size = (t % 1000) + 50;
            sprintf(url, "https://picsum.photos/%d" size);
            downloadImage(url, TIMESTAMP);
            sleep(5);
        }
```

c. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).
```bash
void zip(char* foldername){
    char output[30];
    sprintf(output, "%s.zip", foldername);

    pid_t id = fork();
    if(id==0){
        execlp("zip", "zip", output, foldername, NULL);
        exit(EXIT_FAILURE);
    }
    else if(id > 0 ){
        int status;
        waitpid(id, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
                printf("Folder %s berhasil di zip.\n", foldername);
            } else {
                printf("Gagal mengzip folder %s.\n", foldername);
            }
        
        id = fork();
        if(id==0){
            execlp("rm", "rm", "-r", foldername, NULL);
            exit(EXIT_FAILURE);
        }
        else if(id > 0 ){
            int status;
            waitpid(id, &status, 0);
            if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
                printf("Folder %s berhasil di zip.\n", foldername);
            } else {
                printf("Gagal mengzip folder %s.\n", foldername);
            }
        }   
    }
}
```
```bash
zip(TIMESTAMP);
        sleep(30);
    }
```

d. Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

e. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

Catatan :
Tidak boleh menggunakan system()
Proses berjalan secara daemon
Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

Screenshots 

![image](https://cdn.discordapp.com/attachments/853191323589541908/1094273729153994803/WhatsApp_Image_2023-04-08_at_21.51.48.jpeg)
![image](https://cdn.discordapp.com/attachments/853191323589541908/1094273728633897100/WhatsApp_Image_2023-04-08_at_21.51.49.jpeg)
![image](https://cdn.discordapp.com/attachments/853191323589541908/1094273728910729326/WhatsApp_Image_2023-04-08_at_21.51.50.jpeg)


# Nomor 3
a. Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.

```bash
    // Download players.zip
    pid_t pid;
    char *dir_name = "players";

    pid = fork();
    if (pid == 0) {
        execl("/usr/bin/wget", "wget", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", "-O", "players.zip", NULL);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        wait(NULL);
    } else {
        perror("fork");
        exit(EXIT_FAILURE);
    }
```
untuk mendownload disini kami menggunakan fungsi execl untuk menjalankan perintah `"/usr/bin/wget", "wget", "link"` untuk mendownload file yang ada pada link, kemudian ada perintah `"-O"` sebagai opsi untuk menentukan nama file hasil download, dan `players.zip` sebagai nama file hasil download.

```bash
    // Extract players.zip
    pid = fork();
    if (pid == 0) {
        execlp("unzip", "unzip", "players.zip", NULL);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        wait(NULL);
    } else {
        perror("fork");
        exit(EXIT_FAILURE);
    }
```
Untuk mengekstrak file zip, kami menggunakan fungsi `execlp unzip` dan menjalankan perintah unzip untuk file yang bernama `players.zip`.

```bash
    // Remove players.zip
    pid = fork();
    if (pid == 0) {
        execlp("rm", "rm", "players.zip", NULL);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        wait(NULL);
    } else {
        perror("fork");
        exit(EXIT_FAILURE);
    }
```
dan untuk melakukan remove file zip kami menggunakan fungsi `execlp rm` dan menjalankan perintah remove untuk file yang bernama `players.zip`.


b. Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  

```bash
    // Mencari file-file yang mengandung kata "ManUtd" pada direktori "players"
    DIR *dir;
    struct dirent *ent;

    // membuka direktori "players"
    if ((dir = opendir ("players")) != NULL) {
        // loop untuk membaca setiap file dalam direktori
        while ((ent = readdir (dir)) != NULL) {
            // memeriksa apakah file memiliki ekstensi .png
            char *dot = strrchr(ent->d_name, '.');
            if (dot && !strcmp(dot, ".png")) {
                // memeriksa apakah file tidak mengandung kata "ManUtd"
                if (strstr(ent->d_name, "_ManUtd_") == NULL) {
                    // membuat child process
                    pid_t pid = fork();

                    if (pid == 0) {
                        // child process
                        char players[264];
                        sprintf(players, "players/%s", ent->d_name);
                        execlp("rm", "rm", players, NULL);
                        exit(EXIT_SUCCESS);
                    } else if (pid < 0) {
                        // error
                        perror("fork failed");
                        exit(EXIT_FAILURE);
                    }
                }
            }
        }
    }
```
Kode di atas merupakan bagian dari program yang bertujuan untuk mencari dan 
menghapus file-file dengan ekstensi `.png` yang tidak mengandung kata `"ManUtd"`
pada direktori `players` yang telah di-ekstrak dari file players.zip sebelumnya 
menggunakan fork dan exec.

Pada bagian ini, program akan membuka direktori `players` menggunakan fungsi
`opendir()` dan melakukan loop untuk membaca setiap file yang ada di dalamnya 
menggunakan fungsi `readdir()`. Kemudian, program memeriksa apakah file memiliki 
ekstensi `.png` dengan menggunakan fungsi `strrchr()` untuk mencari titik terakhir 
dalam nama file dan memeriksa string setelah titik. 

Jika file tersebut memiliki ekstensi `.png`, maka program memeriksa apakah file tidak mengandung 
kata `"ManUtd"` dengan menggunakan fungsi `strstr().` Jika file tersebut tidak mengandung 
kata `"ManUtd"`, maka program akan membuat child process dengan menggunakan `fork()` dan 
menjalankan perintah `rm` pada file tersebut menggunakan `execlp()`. 
Fungsi `sprintf()` digunakan untuk menyusun path file yang akan dihapus dari direktori `players` 
dan nama file yang telah dibaca. Setelah itu, program keluar dari child process menggunakan `exit()`. 
Jika `fork()` gagal, maka program mengeluarkan pesan error menggunakan `perror()` dan keluar dari program 
menggunakan `exit()`.


c. Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.

Akan dibuat folder untuk setiap posisi pemain yaitu "Kiper", "Bek", "Gelandang", dan "Penyerang". Setiap folder dibuat dengan menggunakan fungsi `mkdir` pada direktori kerja saat itu. Proses untuk membuat folder 
dilakukan dengan menggunakan perulangan "for" sebanyak 4 kali, dimana pada setiap iterasinya, dilakukan pembuatan folder dengan menggunakan posisi yang disimpan pada array "positions".

Pertama-tama, array positions dibuat dengan empat elemen yang masing-masing merepresentasikan satu posisi pemain

```bash
    char *positions[4] = {"Kiper", "Bek", "Gelandang", "Penyerang"};
    // membuat folder untuk setiap posisi dengan fork dan exec
    for (int i = 0; i < 4; i++) {
        pid_t pid = fork();
        if (pid == 0) {
            // dalam child process
            execl("/bin/mkdir", "mkdir", positions[i], NULL);
            exit(0);
        }
    }
```
Loop pertama digunakan untuk membuat folder untuk setiap posisi dengan melakukan `fork()` dan menjalankan `execl()` dengan argumen `/bin/mkdir, mkdir,` dan nama posisi.

```bash
    for (int i = 0; i < 4; i++) {
        pid_t pid = fork();
        if (pid == 0) {
            searchAndMove(positions[i]);
            exit(0);
        }
    }
```
Loop kedua juga menjalankan fungsi `searchAndMove()` untuk mencari file dengan ekstensi `.png` yang terdapat di dalam direktori `players`, lalu memindahkan file pemain di direktori `players` yang cocok dengan posisi dan memindahkan file-file tersebut ke dalam folder yang sesuai dengan argumen nama posisi sebagai parameter tersebut ke direktori yang sesuai dengan nama posisi pemain. 

```bash
    for (int i = 0; i < 4; i++) {
        wait(NULL);
    }
```
Loop ketiga menggunakan fungsi `wait(NULL)` untuk menunggu semua child process selesai dieksekusi. Hal ini dilakukan untuk memastikan bahwa parent process menunggu semua child process selesai dieksekusi sebelum program diakhiri dengan nilai 0. Setelah fork dan exec digunakan untuk membuat semua folder, program menunggu 
setiap child process selesai dengan menggunakan `wait(NULL)` dan kemudian keluar.

```bash
    void moveFile(char *filename, char *position)
```
Kode diatas merupakan sebuah fungsi bernama moveFile yang digunakan untuk memindahkan file dari direktori players ke direktori tujuan yang ditentukan oleh position. Fungsi tersebut mengambil dua parameter yaitu `filename` yang merupakan nama file yang akan dipindahkan dan `position` yang merupakan direktori tujuan.

```bash
    char source[264], dest[264];
```
Pertama-tama, fungsi ini mendeklarasikan dua variabel source dan dest bertipe char dengan ukuran 264 karakter.

```bash
    snprintf(source, sizeof(source), "players/%s", filename);
```
Kemudian, menggunakan fungsi snprintf, variabel source diisi dengan nilai dari string `players/filename`. 

```bash
    snprintf(dest, sizeof(dest), "%s/%s", position, filename);
```
Sedangkan variabel dest akan diisi dengan nilai dari string position/ dan filename.

```bash
    execlp("mv", "mv", source, dest, NULL);
```
Setelah itu, fungsi execlp dipanggil dengan tiga parameter yaitu perintah yang akan dijalankan, argumen untuk perintah tersebut, dan nilai NULL sebagai penanda akhir dari daftar argumen. Fungsi execlp ini digunakan untuk mengeksekusi perintah `mv(move)` dengan argumen source sebagai file sumber dan dest sebagai file tujuan. 
Setelah selesai, program akan berakhir.

```bash
    void searchAndMove(char *position) {
        DIR *dir;
        struct dirent *ent;
        dir = opendir("players");
        if (dir != NULL) {
            while ((ent = readdir(dir)) != NULL)
```
Kode di atas merupakan implementasi dari fungsi `searchAndMove` yang mencari file gambar dalam direktori `players`, kemudian memindahkan file tersebut ke direktori yang sesuai berdasarkan nama file tersebut.
Pada awalnya, fungsi membuka direktori players dengan menggunakan fungsi `opendir()`. Selanjutnya, fungsi melakukan iterasi pada setiap file dalam direktori tersebut dengan menggunakan fungsi `readdir()`.

```bash
    if (strstr(ent->d_name, ".png") != NULL)
```
Kode ini berfungsi untuk mencari file dengan ekstensi `.png` pada direktori `players`.

```bash
    if (strstr(ent->d_name, "ManUtd_Kiper") != NULL) 
```
digunakan untuk mencari string `ManUtd_Kiper` pada nama file dalam variabel `ent->d_name`. Jika string tersebut 
ditemukan, maka `pid_t pid = fork();` akan membuat proses child baru.

```bash
    moveFile(ent->d_name, "Kiper");
```
Kemudian, dalam proses child tersebut, fungsi `moveFile(ent->d_name, "Kiper");` akan dipanggil untuk memindahkan file ke direktori `Bek` yang sudah ditentukan sebelumnya. Setelah itu, proses child akan keluar menggunakan `exit(0);`. Proses parent akan terus berjalan mencari file lainnya pada direktori `players`.

```bash
    if (strstr(ent->d_name, "ManUtd_Bek") != NULL) 
```
digunakan untuk mencari string `ManUtd_Bek` pada nama file dalam variabel `ent->d_name`. Jika string tersebut 
ditemukan, maka `pid_t pid = fork();` akan membuat proses child baru.

```bash
    moveFile(ent->d_name, "Bek");
```
Kemudian, dalam proses child tersebut, fungsi `moveFile(ent->d_name, "Bek");` akan dipanggil untuk memindahkan file ke direktori `Bek` yang sudah ditentukan sebelumnya. Setelah itu, proses child akan keluar menggunakan `exit(0);`. Proses parent akan terus berjalan mencari file lainnya pada direktori `players`.

```bash
    if (strstr(ent->d_name, "ManUtd_Gelandang") != NULL) 
```
digunakan untuk mencari string `ManUtd_Gelandang` pada nama file dalam variabel `ent->d_name`. Jika string tersebut ditemukan, maka `pid_t pid = fork();` akan membuat proses child baru.

```bash
    moveFile(ent->d_name, "Gelandang");
```
Kemudian, dalam proses child tersebut, fungsi `moveFile(ent->d_name, "Gelandang");` akan dipanggil untuk memindahkan file ke direktori `Gelandang` yang sudah ditentukan sebelumnya. Setelah itu, proses child akan keluar menggunakan `exit(0);`. Proses parent akan terus berjalan mencari file lainnya pada direktori `players`.

```bash
    if (strstr(ent->d_name, "ManUtd_Penyerang") != NULL) 
```
digunakan untuk mencari string `ManUtd_Penyerang` pada nama file dalam variabel `ent->d_name`. Jika string tersebut ditemukan, maka `pid_t pid = fork();` akan membuat proses child baru.

```bash
    moveFile(ent->d_name, "Penyerang");
```
Kemudian, dalam proses child tersebut, fungsi `moveFile(ent->d_name, "Penyerang");` akan dipanggil untuk memindahkan file ke direktori `Penyerang` yang sudah ditentukan sebelumnya. Setelah itu, proses child akan keluar menggunakan `exit(0);`. Proses parent akan terus berjalan mencari file lainnya pada direktori `players`.

```bash
    closedir(dir);
```
Fungsi akan terus melakukan iterasi pada setiap file dalam direktori `players` sampai semua file selesai diperiksa. Setelah selesai, fungsi akan menutup direktori players dengan menggunakan fungsi `closedir()`.


d. Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

Catatan:
-Format nama file yang akan diunduh dalam zip dan isi txt formasi berupa [nama]_[tim]_[posisi]_[rating].png
-Tidak boleh menggunakan system()
-Tidak boleh memakai function C mkdir() ataupun rename().
-Gunakan exec() dan fork().
-Directory “.” dan “..” tidak termasuk yang akan dihapus.
-Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.

screenshots

![image](https://cdn.discordapp.com/attachments/853191323589541908/1094269334613921902/Screenshot_44.png)
![image](https://cdn.discordapp.com/attachments/853191323589541908/1094269335033368656/Screenshot_46.png)
![image](https://cdn.discordapp.com/attachments/853191323589541908/1094269335310172221/Screenshot_47.png)



# Nomor 4
-Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.

-Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh

```bash
    char cron[1024];
    char file_name[100];
    int len;

```
inisiasi variabel dan array untuk menyimpan cron dan filename atau direktori
```bash
    fgets(cron, 1024, stdin);
    scanf("%s", file_name);
```
untuk mengambil input 

```bash
        len = strlen(file_name);
    if (len > 3 && strcmp(file_name + len - 3, ".sh") == 0) {
        printf("directory is valid.\n");
    } else {
        printf("eror directory is not valid.\n");
    }
```
`strcmp(file_name + len - 3, ".sh")` digunakan untuk memeriksa input file_name sebagai direktori atau program yang memiliki tiga karakter terakhir .sh 

```bash
    while (p != NULL && i < 5) {
        fields[i++] = p;
        p = strtok(NULL, " \t\n");
    }
    if (i < 5) {
        printf("eror cron expression\n");
        return 1;
    }
```
menggunakan strok untuk memeriksa input cron yakni 5 karakter yang dipisahkan oleh spasi atau tab, jika kurang atau lebih dari lima maka eror

```bash
int start = (strcmp(fields[i], "*") == 0) ? 0 : strtol(fields[i], NULL, 10);
            for (int j = start; j < 60; j += step) {
                values[i][j] = 1;
            }
            
        }
```
untuk memvalidasi nilai cron berupa tanda asteriks jika digunakan maka bernilai satu pada array
```bash
int value = strtol(fields[i], NULL, 10);
            if (value < 0 || (i == 0 && value > 59) || (i == 1 && value > 23) || (i == 2 && value > 31) || 
            (i == 3 && value > 12) || (i == 4 && value > 6)) {
                printf("eror cron expression\n");
                return 1;
            }
            values[i][value] = 1;
```
untuk memeriksa cron sesuai dengan argumen yang dapat diterima, yakni menit, jam, hari, bulan, dan hari per minggu 

-Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.

Pesan eror yang dibuat sesuai dengan kondisi, disini jika file input direktori tidak mengandung .sh diakhir maka akan langsung dianggap eror. Begitu juga dengan cron dimana akan eror jika tidak sesuai dengan condition yang telah dibuat seperti input ron harus 5, dan dapat mengandung asteriks. dan maksimum angka yang dimasukkan.

-Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.

didalam file ini telah tersedia fungsi untuk menjalankan secara daemon. namun, karena program yang kami pakai hanya menggunakan C stdin dan stdout maka tidak dapat berjalan secara daemon. Juga karena mengandalkan input fgets dan scan maka terlebih dahulu harus meng enter agar dapat memasukkan input berupa cron atau direktori file. dan tidak dapat mengeksekusi program dengan satu line ./program input

Screenshot contoh input valid atau tidak valid

![image](https://cdn.discordapp.com/attachments/853191323589541908/1094247385040965712/image.png)

![image](https://cdn.discordapp.com/attachments/853191323589541908/1094247850055057488/image.png)

![image](https://cdn.discordapp.com/attachments/853191323589541908/1094258620004311070/image.png)

![image](https://cdn.discordapp.com/attachments/853191323589541908/1094259304711852122/image.png)
proses yang berjalan ketika program dieksekusi


# Hambatan dan Kesulitan Dalam Praktikum


Pada soal4 ditemui kesulitan karena harus mengatur input agar dapar dieksekusi dengan satu baris ./program <input> 
kami sudah mencoba menggunakan argv namun terjadi eror, begitu juga ketika diperintahkan untuk berjalan secara daemon kami baru menyadari stdin tidak dapat digunakan secara daemon. Kemudian masalah lain kurangnya mengenali beberapa fungsi didalam c untuk menjalankan command linux yang sesuai. 

