#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>

void moveFile(char *filename, char *position);
void searchAndMove(char *position);

int main() {
    // Download players.zip
    pid_t pid;
    char *dir_name = "players";

    pid = fork();
    if (pid == 0) {
        execl("/usr/bin/wget", "wget", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", "-O", "players.zip", NULL);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        wait(NULL);
    } else {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    // Extract players.zip
    pid = fork();
    if (pid == 0) {
        execlp("unzip", "unzip", "players.zip", NULL);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        wait(NULL);
    } else {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    // Remove players.zip
    pid = fork();
    if (pid == 0) {
        execlp("rm", "rm", "players.zip", NULL);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        wait(NULL);
    } else {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    
    // Mencari file-file yang mengandung kata "ManUtd" pada direktori "players"
    DIR *dir;
    struct dirent *ent;

    // membuka direktori "players"
    if ((dir = opendir ("players")) != NULL) {
        // loop untuk membaca setiap file dalam direktori
        while ((ent = readdir (dir)) != NULL) {
            // memeriksa apakah file memiliki ekstensi .png
            char *dot = strrchr(ent->d_name, '.');
            if (dot && !strcmp(dot, ".png")) {
                // memeriksa apakah file tidak mengandung kata "ManUtd"
                if (strstr(ent->d_name, "_ManUtd_") == NULL) {
                    // membuat child process
                    pid_t pid = fork();

                    if (pid == 0) {
                        // child process
                        char players[264];
                        sprintf(players, "players/%s", ent->d_name);
                        execlp("rm", "rm", players, NULL);
                        exit(EXIT_SUCCESS);
                    } else if (pid < 0) {
                        // error
                        perror("fork failed");
                        exit(EXIT_FAILURE);
                    }
                }
            }
        }
    }

    char *positions[4] = {"Kiper", "Bek", "Gelandang", "Penyerang"};
    // membuat folder untuk setiap posisi dengan fork dan exec
    for (int i = 0; i < 4; i++) {
        pid_t pid = fork();
        if (pid == 0) {
            // dalam child process
            execl("/bin/mkdir", "mkdir", positions[i], NULL);
            exit(0);
        }
    }
    for (int i = 0; i < 4; i++) {
        pid_t pid = fork();
        if (pid == 0) {
            searchAndMove(positions[i]);
            exit(0);
        }
    }
    for (int i = 0; i < 4; i++) {
        wait(NULL);
    }
    return 0;
}

void moveFile(char *filename, char *position) {
    char source[264], dest[264];
    snprintf(source, sizeof(source), "players/%s", filename);
    snprintf(dest, sizeof(dest), "%s/%s", position, filename);
    execlp("mv", "mv", source, dest, NULL);
}

void searchAndMove(char *position) {
    DIR *dir;
    struct dirent *ent;
    dir = opendir("players");
    if (dir != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (strstr(ent->d_name, ".png") != NULL) {
                if (strstr(ent->d_name, "ManUtd_Kiper") != NULL) {
                    pid_t pid = fork();
                    if (pid == 0) {
                        moveFile(ent->d_name, "Kiper");
                        exit(0);
                    }
                }
                else if (strstr(ent->d_name, "ManUtd_Bek") != NULL) {
                    pid_t pid = fork();
                    if (pid == 0) {
                        moveFile(ent->d_name, "Bek");
                        exit(0);
                    }
                }
                else if (strstr(ent->d_name, "ManUtd_Gelandang") != NULL) {
                    pid_t pid = fork();
                    if (pid == 0) {
                        moveFile(ent->d_name, "Gelandang");
                        exit(0);
                    }
                }
                else if (strstr(ent->d_name, "ManUtd_Penyerang") != NULL) {
                    pid_t pid = fork();
                    if (pid == 0) {
                        moveFile(ent->d_name, "Penyerang");
                        exit(0);
                    }
                }
            }
        }
        closedir(dir);
    } 
}
