#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <curl/curl.h>
#include <unistd.h>
#include <sys/stat.h>


void createFolder(char TIMESTAMP[]){
    mkdir(TIMESTAMP, 0777);
}

void downloadImage(char* url, char* foldername) {
    time_t t = time(NULL); 
    time_t current_time;
    time(&current_time);
    char path[200];
    char command[500];

    char TIMESTAMP[22];
    strftime(TIMESTAMP, sizeof(TIMESTAMP), "[%Y-%m-%d_%H:%M:%S]", localtime(&current_time));

    char filename[30];
    sprintf(filename, "%s.jpg", TIMESTAMP);

    sprintf(path, "%s/%s", foldername, filename);

    pid_t id = fork();
    if(id==0){
        execlp("wget", "wget", url, "-O", path, NULL);
        exit(0);
    }
    else if(id > 0){
        int status;
        waitpid(id, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("File berhasil diunduh\n");
        } else {
            printf("File gagal diunduh\n");
        }
    }
    
}

void zip(char* foldername){
    char output[30];
    sprintf(output, "%s.zip", foldername);

    pid_t id = fork();
    if(id==0){
        execlp("zip", "zip", output, foldername, NULL);
        exit(EXIT_FAILURE);
    }
    else if(id > 0 ){
        int status;
        waitpid(id, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
                printf("Folder %s berhasil di zip.\n", foldername);
            } else {
                printf("Gagal mengzip folder %s.\n", foldername);
            }
        
        id = fork();
        if(id==0){
            execlp("rm", "rm", "-r", foldername, NULL);
            exit(EXIT_FAILURE);
        }
        else if(id > 0 ){
            int status;
            waitpid(id, &status, 0);
            if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
                printf("Folder %s berhasil di zip.\n", foldername);
            } else {
                printf("Gagal mengzip folder %s.\n", foldername);
            }
        }   
    }
}


int main() {

    while(1){
        time_t t = time(NULL); 
        time_t current_time;
        time(&current_time);
        char TIMESTAMP[22];
        strftime(TIMESTAMP, sizeof(TIMESTAMP), "[%Y-%m-%d_%H:%M:%S]", localtime(&current_time));

        createFolder(TIMESTAMP);
        
        int i;
        for(i = 0; i<15; i++){
            

            char url[50];
            int size = (t % 1000) + 50;
            sprintf(url, "https://picsum.photos/%d", size);
            downloadImage(url, TIMESTAMP);
            sleep(5);
        }
        zip(TIMESTAMP);
        sleep(30);
    }
    
    return 0;
}
